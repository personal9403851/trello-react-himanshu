import React, { useEffect, useState } from "react";
import axios from "axios";
import { Checkbox, Text } from "@radix-ui/themes";
import DialogComponent from "../Components/DialogComponent/DialogComponent";
import { Spinner, Progress } from "@chakra-ui/react";

function DisplayCheckItems({ checkListId, cardId }) {
  const [checkItems, setCheckItems] = useState([]);
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [loading, setLoading] = useState(true);

  const getCheckItems = () => {
    axios
      .get(
        `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        setCheckItems(res.data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  const deleteCheckItem = (checkItemId) => {
    axios
      .delete(
        `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        const newCheckItems = checkItems.filter((eachCheckItem) => {
          if (eachCheckItem.id != checkItemId) return true;
        });
        setCheckItems(newCheckItems);
      });
  };

  const updateCheckbox = (checkItem) => {
    let newState = checkItem.state === "incomplete" ? "complete" : "incomplete";

    setCheckItems((prev) => {
      return prev.map((item) => {
        if (item.id === checkItem.id) {
          return { ...item, state: newState };
        } else {
          return item;
        }
      });
    });

    axios
      .put(
        `https://api.trello.com/1/cards/${cardId}/checklist/${checkListId}/checkItem/${checkItem.id}?key=${apiKey}&token=${token}&state=${newState}`
      )
      .then((res) => console.log(res))
      .catch((error) => console.log(error));
  };

  let totalCheckItems = checkItems.length;
  let completedCheckItems = 0;
  let progressValue = 0;

  if (checkItems.length > 0) {
    checkItems.map((eachCheckItem) => {
      if (eachCheckItem.state === "complete") completedCheckItems++;
    });

    progressValue = (completedCheckItems / totalCheckItems) * 100;
  }

  useEffect(() => {
    getCheckItems();
  }, []);

  if (loading) {
    return (
      <div className=" flex justify-center w-full">
        <Spinner color="white" />;
      </div>
    );
  }
  console.log("asdasd", checkItems);
  return (
    <div className="check-items-container flex-col rounded-md bg-trello-transparent text-white p-1">
      {checkItems.length === 0 ? (
        <p className=" text-xs">No checkitem to display</p>
      ) : (
        checkItems.map((eachCheckItem) => {
          return (
            <div
              key={eachCheckItem.id}
              className="each-check-item flex-col justify-between my-2 px-2"
            >
              <div className="flex justify-between mb-5">
                <Text as="label" size="2">
                  <Checkbox
                    checked={
                      eachCheckItem.state === "incomplete" ? false : true
                    }
                    onClick={() => updateCheckbox(eachCheckItem)}
                  />
                </Text>
                <p>{eachCheckItem.name}</p>
                <button
                  onClick={() => deleteCheckItem(eachCheckItem.id)}
                  className=" text-base"
                >
                  ×
                </button>
              </div>
            </div>
          );
        })
      )}
      <Progress className=" rounded-lg" value={progressValue} />

      <DialogComponent
        name={"checkItem"}
        id={checkListId}
        state={checkItems}
        setState={setCheckItems}
      />
    </div>
  );
}

export default DisplayCheckItems;
