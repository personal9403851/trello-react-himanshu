import React, { useEffect, useState } from "react";
import axios from "axios";
import { Flex } from "@radix-ui/themes";
import { Link } from "react-router-dom";
import AlertDialogComponent from "../AlertDialogComponent/AlertDialogComponent";
import DialogComponent from "../DialogComponent/DialogComponent";
import { Spinner } from "@chakra-ui/react";

function Boards() {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [boards, setBoards] = useState("");
  const [loading, setLoading] = useState(true);

  const fetchBoardsData = () => {
    axios
      .get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        setBoards(res.data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    fetchBoardsData();
  }, []);

  if (loading) {
    return(
    <div className=" flex justify-center w-full">
    <Spinner color="white" />;
    </div>
    )
  }

  return (
    <div className="flex justify-between w-full">
      {boards.length === 0 ? (
        <p className=" text-white w-full text-center">No boards to display</p>
      ) : (
        <Flex gap="2" className="flex flex-wrap mx-2">
          {boards.map((eachBoard) => (
            <div
              key={eachBoard.id}
              className="flex-column h-40 w-48 text-gray-100 bg-gray-600 font-semibold rounded-md shadow-2xl p-2"
            >
              <AlertDialogComponent
                name={"board"}
                id={eachBoard.id}
                state={boards}
                setState={setBoards}
              />
              <Link to={`/board/${eachBoard.id}`}>
                <div className="h-28 flex justify-center items-center">
                  <span className="text-center">{eachBoard.name}</span>
                </div>
              </Link>
            </div>
          ))}
        </Flex>
      )}
      <DialogComponent
        name={"board"}
        id={""}
        state={boards}
        setState={setBoards}
      />
    </div>
  );
}

export default Boards;
