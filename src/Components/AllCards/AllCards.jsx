import axios from "axios";
import React, { useEffect, useState } from "react";
import { Dialog } from "@radix-ui/themes";
import DisplayCheckList from "../DisplayCheckList/DisplayCheckList";
import AlertDialogComponent from "../AlertDialogComponent/AlertDialogComponent";
import DialogComponent from "../DialogComponent/DialogComponent";
import { Spinner } from "@chakra-ui/react";

function AllCards({ lid }) {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [someCards, setSomeCards] = useState([]);
  const [loading, setLoading] = useState(true);

  const getCards = () => {
    axios
      .get(
        `https://api.trello.com/1/lists/${lid}/cards?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        setSomeCards(res.data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getCards();
  }, []);

  if (loading) {
    return (
      <div className=" flex justify-center w-full">
        <Spinner color="white" />
      </div>
    );
  }

  return (
    <div className="cards-outer-container">
      <div className="cards-container">
        {someCards.length === 0 ? (
          <p className=" text-white text-center w-full">No cards to display</p>
        ) : (
          someCards.map((eachCard) => {
            return (
              <div key={eachCard.id} className=" bg-slate-400 text-black flex justify-between rounded-sm px-2 cursor-pointer">
                <Dialog.Root>
                  <Dialog.Trigger>
                    <p
                      className="py-1 bg-inherit text-black w-3/4"
                    >
                      {eachCard.name}
                    </p>
                  </Dialog.Trigger>
                  <Dialog.Content style={{ maxWidth: 450 }}>
                    <Dialog.Title>{eachCard.name}</Dialog.Title>
                    <DisplayCheckList cardId={eachCard.id} />
                  </Dialog.Content>
                </Dialog.Root>
                <AlertDialogComponent
                  name={"card"}
                  id={eachCard.id}
                  state={someCards}
                  setState={setSomeCards}
                />
              </div>
            );
          })
        )}
      </div>
      <span>
        <DialogComponent
          name={"card"}
          id={lid}
          state={someCards}
          setState={setSomeCards}
        />
      </span>
    </div>
  );
}

export default AllCards;
