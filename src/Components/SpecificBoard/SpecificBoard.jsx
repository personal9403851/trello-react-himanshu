import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import { Flex } from "@radix-ui/themes";
import AlertDialogComponent from "../AlertDialogComponent/AlertDialogComponent";
import AllCards from "../AllCards/AllCards";
import DialogComponent from "../DialogComponent/DialogComponent";
import LeftPane from "../LeftPane/LeftPane";
import { Spinner } from "@chakra-ui/react";

function SpecificBoard() {
  const [boardList, setBoardList] = useState("");
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const { id } = useParams();
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  const getBoardList = () => {
    axios
      .get(
        `https://api.trello.com/1/boards/${id}/lists?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        setBoardList(res.data);
        setLoading(false);
      })
      .catch((error) => {
        console.log("fkldsfj", error);
        setError(true);
      });
  };

  useEffect(() => {
    getBoardList();
  }, []);

  if (loading) {
    return (
      <div className=" flex justify-center w-full mt-2">
        <Spinner color="white" />;
      </div>
    );
  }

  return error ? (
    <p className=" text-white text-center w-full">No such board</p>
  ) : (
    <div className="flex justify-between mx-20 mt-5">
      <LeftPane />
      <Flex className=" flex-wrap w-full">
        {boardList.length === 0 ? (
          <p className="text-white w-full text-center">No list to display</p>
        ) : (
          boardList.map((eachList) => {
            return (
              <div key={eachList.id} className="specific-div-card">
                <Flex justify={"between"}>
                  <p className="list-name">{eachList.name}</p>
                  <AlertDialogComponent
                    name={"list"}
                    id={eachList.id}
                    state={boardList}
                    setState={setBoardList}
                  />
                </Flex>
                <AllCards lid={eachList.id} />
              </div>
            );
          })
        )}
      </Flex>
      <DialogComponent
        name={"list"}
        id={id}
        state={boardList}
        setState={setBoardList}
      />
    </div>
  );
}

export default SpecificBoard;
