import React from "react";

function LeftPane() {
  return (
    <div className="left-pane px-20 text-slate-300">
      <div className="upper font-semibold">
        <div>Boards</div>
        <div>Templates</div>
        <div>Home</div>
      </div>
    </div>
  );
}

export default LeftPane;
