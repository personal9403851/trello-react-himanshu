import React from "react";

function Error() {
  return (
    <div className=" text-white text-center w-full">
      Sorry, unexpected error occurred.
    </div>
  );
}

export default Error;
