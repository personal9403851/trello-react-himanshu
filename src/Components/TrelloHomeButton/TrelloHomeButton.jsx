import { Button, Avatar } from "@radix-ui/themes";
import TrelloIcon from "../../../public/Trello/SVG/logo-gradient-white-trello.svg";
import React from "react";

function MyApp() {
  return (
    <Button className="cursor-pointer">
      <Avatar src={TrelloIcon} size="10" className=" h-3"></Avatar>
    </Button>
  );
}

export default MyApp;
