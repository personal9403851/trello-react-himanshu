import React, { useState } from "react";
import { Dialog, Button, Flex, TextField, Text } from "@radix-ui/themes";
import axios from "axios";

function DialogComponent({ name, id, state, setState }) {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  const [feedName, setFeedName] = useState("");

  const createNewItem = (e) => {
    e.preventDefault();
    if (name === "board") {
      axios
        .post(
          `https://api.trello.com/1/boards/?name=${feedName}&key=${apiKey}&token=${token}`
        )
        .then((res) => {
          setState([...state, res.data]);
        })
        .catch((error) => console.log(error));
    }

    if (name === "list") {
      axios
        .post(
          `https://api.trello.com/1/lists?name=${feedName}&idBoard=${id}&key=${apiKey}&token=${token}`
        )
        .then((res) => {
          setState([...state, res.data]);
        })
        .catch((error) => console.log(error));
    }

    if (name === "card") {
      axios
        .post(
          `https://api.trello.com/1/cards?idList=${id}&name=${feedName}&key=${apiKey}&token=${token}`
        )
        .then((res) => {
          setState([...state, res.data]);
        })
        .catch((error) => console.log(error));
    }

    if (name === "checklist") {
      axios
        .post(
          `https://api.trello.com/1/cards/${id}/checklists?name=${feedName}&key=${apiKey}&token=${token}`
        )
        .then((res) => {
          setState([...state, res.data]);
        })
        .catch((error) => console.log(error));
    }

    if (name === "checkItem") {
      axios
        .post(
          `https://api.trello.com/1/checklists/${id}/checkItems?name=${feedName}&key=${apiKey}&token=${token}`
        )
        .then((res) => {
          setState([...state, res.data]);
        })
        .catch((error) => console.log(error));
    }
  };

  return (
    <Dialog.Root>
      <Dialog.Trigger className=" bg-cyan-700 mt-1">
        <Button className=" cursor-pointer">{`Create new ${name}`}</Button>
      </Dialog.Trigger>

      <Dialog.Content style={{ maxWidth: 450 }}>
        <form onSubmit={(e) => createNewItem(e)}>
          <Dialog.Title>{`Create ${name}`}</Dialog.Title>
          <Dialog.Description size="2" mb="4">
            {`Give name for your ${name}`}
          </Dialog.Description>

          <Flex direction="column" gap="3">
            <label>
              <Text as="div" size="2" mb="1" weight="bold">
                Name
              </Text>
              <TextField.Input
                placeholder="eg. Progress"
                value={feedName}
                onChange={(e) => setFeedName(e.target.value)}
              />
            </label>
          </Flex>

          <Flex gap="3" mt="4" justify="end">
            <Dialog.Close>
              <Button variant="soft" color="gray" className=" cursor-pointer">
                Cancel
              </Button>
            </Dialog.Close>
            <Dialog.Close>
              <Button type="submit" className=" bg-cyan-700 cursor-pointer">
                Save
              </Button>
            </Dialog.Close>
          </Flex>
        </form>
      </Dialog.Content>
    </Dialog.Root>
  );
}

export default DialogComponent;
