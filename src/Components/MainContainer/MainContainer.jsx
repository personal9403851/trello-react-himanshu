import React from "react";
import Boards from "../Boards/Boards";
import { Flex } from "@radix-ui/themes";
import LeftPane from "../LeftPane/LeftPane";

function MainContainer() {
  return (
    <Flex className=" px-5 py-3 justify-between">
      <LeftPane />
      <Boards />
    </Flex>
  );
}

export default MainContainer;
