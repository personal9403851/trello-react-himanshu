import React from "react";
import { AlertDialog, Button, Flex } from "@radix-ui/themes";
import axios from "axios";

function AlertDialogComponent({
  name,
  id,
  checkItemID = "1",
  state,
  setState,
}) {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;

  const removeItem = () => {
    if (name === "board") {
      axios
        .delete(
          `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${token}`
        )
        .then((res) => {
          const newBoards = state.filter((eachBoard) => {
            if (eachBoard.id != id) return true;
          });
          setState(newBoards);
        })
        .catch((error) => console.log(error));
    }

    if (name === "list") {
      axios
        .put(
          `https://api.trello.com/1/lists/${id}/closed?key=${apiKey}&token=${token}&value=true`
        )
        .then((res) => {
          const newLists = state.filter((eachList) => {
            if (eachList.id != id) return true;
          });

          setState(newLists);
        })
        .catch((error) => console.log(error));
    }

    if (name === "card") {
      axios
        .delete(
          `https://api.trello.com/1/cards/${id}?key=${apiKey}&token=${token}`
        )
        .then((res) => {
          const newCards = state.filter((eachCard) => {
            if (eachCard.id != id) return true;
          });

          setState(newCards);
        })
        .catch((error) => console.log(error));
    }

    if (name === "checklist") {
      axios
        .delete(
          `https://api.trello.com/1/checklists/${id}?key=${apiKey}&token=${token}`
        )
        .then((res) => {
          const newCheckList = state.filter((eachCheckList) => {
            if (eachCheckList.id != id) return true;
          });

          setState(newCheckList);
        })
        .catch((error) => console.log(error));
    }

    if (name === "checkItem") {
      axios
        .delete(
          `https://api.trello.com/1/checklists/${id}/checkItems/${checkItemID}?key=${apiKey}&token=${token}`
        )
        .then((res) => {
          const newCheckItem = state.filter((eachCheckItem) => {
            if (eachCheckItem.id != checkItemID) return true;
          });

          setState(newCheckItem);
        })
        .catch((error) => console.log(error));
    }
  };
  return (
    <AlertDialog.Root>
      <AlertDialog.Trigger>
        <span
          id="delete-board"
          className=" bg-inherit cursor-pointer text-[20px]"
        >
          ×
        </span>
      </AlertDialog.Trigger>
      <AlertDialog.Content style={{ maxWidth: 450 }}>
        <AlertDialog.Title>{`Delete ${name}`}</AlertDialog.Title>
        <AlertDialog.Description size="2">
          Are you sure? This item will no longer be accessible.
        </AlertDialog.Description>

        <Flex gap="3" mt="4" justify="end">
          <AlertDialog.Cancel>
            <Button variant="soft" color="gray">
              Cancel
            </Button>
          </AlertDialog.Cancel>
          <AlertDialog.Action>
            <Button
              onClick={() => removeItem()}
              variant="solid"
              color="red"
              className=" bg-red-700 cursor-pointer"
            >
              Delete
            </Button>
          </AlertDialog.Action>
        </Flex>
      </AlertDialog.Content>
    </AlertDialog.Root>
  );
}

export default AlertDialogComponent;
