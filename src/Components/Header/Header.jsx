import React from "react";
import TrelloHomeButton from "../TrelloHomeButton/TrelloHomeButton";
import Search from "../Search/Search";
import { Flex } from "@radix-ui/themes";
import { Link } from "react-router-dom";

function Header() {
  return (
    <Flex justify={"between"} className=" px-5 py-2 border-b border-gray-500">
      <Link to={`/`}>
        <TrelloHomeButton />
      </Link>
      <Search />
    </Flex>
  );
}

export default Header;
