import { TextField } from "@radix-ui/themes";
import { MagnifyingGlassIcon } from "@radix-ui/react-icons";
import React from "react";

function Search() {
  return (
    <TextField.Root radius="medium">
      <TextField.Slot className=" bg-trello-transparent text-gray-300">
        <MagnifyingGlassIcon height="16" width="16" />
      </TextField.Slot>
      <TextField.Input placeholder="Search the docs…" className="abc" />
    </TextField.Root>
  );
}

export default Search;
