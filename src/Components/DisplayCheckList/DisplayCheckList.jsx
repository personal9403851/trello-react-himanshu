import React, { useEffect, useState } from "react";
import axios from "axios";
import DialogComponent from "../DialogComponent/DialogComponent";
import AlertDialogComponent from "../AlertDialogComponent/AlertDialogComponent";
import DisplayCheckItems from "../../DisplayCheckItems/DisplayCheckItems";
import { Spinner } from "@chakra-ui/react";

function DisplayCheckList({ cardId }) {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [loading, setLoading] = useState(true);

  const [checkListData, setCheckListData] = useState("");

  const getCheckListData = () => {
    axios
      .get(
        `https://api.trello.com/1/cards/${cardId}/checklists?key=${apiKey}&token=${token}`
      )
      .then((res) => {
        setCheckListData(res.data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    getCheckListData();
  }, []);

  if (loading) {
    return (
      <div className=" flex justify-center w-full">
        <Spinner color="white" />;
      </div>
    );
  }

  return (
    <div className="checklist-outer-container flex-col gap-4">
      <div className="checklist-container w-full pr-10">
        {checkListData.length === 0 ? (
          <p className=" text-white w-full">No checklist to display</p>
        ) : (
          checkListData.map((eachCheckList) => {
            return (
              <div key={eachCheckList.id} className="checklist mb-1 bg-slate-500 pb-2 rounded-md px-2 text-black">
                <div className="check-list-data flex justify-between w-full py-1">
                  <div>{eachCheckList.name}</div>
                  <AlertDialogComponent
                    name={"checklist"}
                    id={eachCheckList.id}
                    state={checkListData}
                    setState={setCheckListData}
                  />
                </div>
                <div className="my-2">
                  <DisplayCheckItems
                    checkListId={eachCheckList.id}
                    cardId={cardId}
                  />
                </div>
              </div>
            );
          })
        )}
      </div>
      <span>
        <DialogComponent
          name={"checklist"}
          id={cardId}
          state={checkListData}
          setState={setCheckListData}
        />
      </span>
    </div>
  );
}

export default DisplayCheckList;
