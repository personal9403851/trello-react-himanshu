import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
import MainContainer from "./Components/MainContainer/MainContainer.jsx";
import SpecificBoard from "./Components/SpecificBoard/SpecificBoard.jsx";
import Error from "./Components/Error.jsx";

const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route path="/" element={<App />}>
        <Route path="/" element={<MainContainer />} />
        <Route path="board/:id" element={<SpecificBoard />} />
      </Route>
      <Route path="*" element={<Error />} />
    </>
  )
);
ReactDOM.createRoot(document.getElementById("root")).render(
  <RouterProvider router={router} />
);
