import React from "react";
import "./App.css";
import Header from "./Components/Header/Header";
import { Outlet } from "react-router-dom";
import "@radix-ui/themes/styles.css";
import { Theme } from "@radix-ui/themes";
import { ChakraProvider } from "@chakra-ui/react";

function App() {
  return (
    <Theme appearance="dark">
      <ChakraProvider>
        <Header />
        <Outlet />
      </ChakraProvider>
    </Theme>
  );
}

export default App;
