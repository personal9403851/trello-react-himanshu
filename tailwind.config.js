/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'trello-bg': "var(--ds-surface, #161A1D)",
        "trello-transparent": "hsla(206,13.7%,10%,0.9)"
      },
    },
  },
  plugins: [],
}

